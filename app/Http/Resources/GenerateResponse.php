<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class GenerateResponse extends JsonResource {
    public $status;
    public $message;
    public $resource;

    public function __construct($status, $message, $resource) {
        parent::__construct($resource);
        $this->status  = $status;
        $this->message = $message;
        $this->resource = $resource;
    }

    public function toResponse($request) {
        $statusMap = [
            200 => Response::HTTP_OK,
            201 => Response::HTTP_CREATED,
            204 => Response::HTTP_NO_CONTENT,
            400 => Response::HTTP_BAD_REQUEST,
            401 => Response::HTTP_UNAUTHORIZED,
            403 => Response::HTTP_FORBIDDEN,
            404 => Response::HTTP_NOT_FOUND,
            500 => Response::HTTP_INTERNAL_SERVER_ERROR,
        ];

        $statusCode = $statusMap[$this->status] ?? Response::HTTP_INTERNAL_SERVER_ERROR;

        return response()->json([
            'status'  => $this->status,
            'message' => $this->message,
            'data'    => $this->resource
        ], $statusCode);
    }
}
