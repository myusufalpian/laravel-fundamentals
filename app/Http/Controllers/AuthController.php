<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Http\Resources\User as UserResource;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Response;
use App\Http\Resources\GenerateResponse;

use OpenApi\Annotations as OA;


class AuthController extends Controller {
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['signin', 'signup']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * Create a new user.
     */
     /*@OA\Post(
        path="/api/users/signin",
        tags={"Users"},
        summary="Create a new user",
        description="Creates a new user with the provided data",
        @OA\RequestBody(
            required=true,
            description="User data",
            @OA\JsonContent(
                @OA\Property(property="name", type="string", example="John Doe"),
                @OA\Property(property="email", type="string", format="email", example="john@example.com"),
                @OA\Property(property="password", type="string", format="password", example="secret123")
            )
        ),
        @OA\Response(
            response=201,
            description="User created successfully",
            @OA\JsonContent(
                @OA\Property(property="message", type="string", example="User created successfully"),
                @OA\Property(property="user", ref="#/components/schemas/User")
            )
        ),
        @OA\Response(
            response=400,
            description="Bad request",
            @OA\JsonContent(
                @OA\Property(property="message", type="string", example="Invalid data provided")
            )
        )
    )*/
    public function signin() {
        $credentials = request(['email', 'password']);
        if (! $token = auth()->attempt($credentials)) {
            return new GenerateResponse(401, 'Unauthorized', null);
        }
        return new GenerateResponse(200, 'Signin successfully', $this->respondWithToken($token));
    }

    public function signup () {
        $validator = Validator::make(request() -> all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return new GenerateResponse(400, 'Validation error', $validator->errors());
        }
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
        ]);
        return new GenerateResponse(201, 'Signup successfully', null);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me() {
        return new GenerateResponse(200, 'Get data user successfully', auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function signout() {
        auth()->signout();
        return new GenerateResponse(200, 'Signed out successfully', null);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return new GenerateResponse(200, 'Refresh token successfully', $this->respondWithToken(auth()->refresh()));
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token) {
        return [
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
    }
}
