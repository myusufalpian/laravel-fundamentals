<?php

use OpenApi\Annotations as OA;

/**
 * @OA\Get(
 *     path="/api/users",
 *     tags={"Users"},
 *     summary="Get list of users",
 *     @OA\Response(
 *         response=200,
 *         description="Successful operation",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/User")
 *         ),
 *     ),
 * )
 */
function index() {
    return [

        /*
        |--------------------------------------------------------------------------
        | L5 Swagger specific settings
        |--------------------------------------------------------------------------
        */

        'api' => [
            /*
             * The name of the generated documentation
             */
            'title' => 'Fundamentals Laravel API',

            /*
             * Edit to set the API's version number
             */
            'version' => '1.0.0',

            /*
             * Edit to set the path to the documentation
             */
            'path' => '/docs',
        ],

        /*
        |--------------------------------------------------------------------------
        | API Information
        |--------------------------------------------------------------------------
        */

        'defaults' => [
            'routes' => [
                /*
                 * Route for accessing api documentation interface
                 */
                'api' => 'api/documentation',
            ],
        ],

        /*
        |--------------------------------------------------------------------------
        | Turn this off to remove swagger generation on production
        |--------------------------------------------------------------------------
        */

        'generate_always' => env('L5_SWAGGER_GENERATE_ALWAYS', true),

        /*
        |--------------------------------------------------------------------------
        | Turn this on if you want to remove the generate command
        |--------------------------------------------------------------------------
        */

        'generate_yaml_copy' => env('L5_SWAGGER_GENERATE_YAML_COPY', false),

        /*
        |--------------------------------------------------------------------------
        | Edit to set the swagger version number
        |--------------------------------------------------------------------------
        */

        'swagger_version' => env('L5_SWAGGER_VERSION', '3.0'),

    ];

}
